#include <string>
#include <vector>
#include <stack>
#include <cstdlib>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstring>

/* maximum command length */
#define CMDSIZE 256
#define PI      3.14159265

using std::vector;
using std::string;
using std::stack;

stack<long double> STACK;
long double reg_a = 0;
long double reg_b = 0;
long double reg_c = 0;
long double reg_d = 0;

int parse(const char *_line, vector<string> *tokens)
{
	int mark = 0;
	string line;
	int i = 0;
	while (*(_line + i) != '\n' && *(_line + i) != '\0') {
		line.push_back(*(_line+i));
		i++;
	}
	string token;
	char c;

	if (line.empty()) {
		return(0);
	}

	while (1) {
		c = line[mark];
		if (c == ' ' || c == '\t') {
			if (!token.empty()) {
				(*tokens).push_back(token);
				token.erase();
			}
		} else if (c == '\0') {
			if (!token.empty()) {
				(*tokens).push_back(token);
			}
			break;
		} else {
			token.push_back(c);
		}
		mark++;
	}
	return(0);
}

int process(vector<string> *tokens)
{
	string       token;
	unsigned int tokens_count;

	if (tokens->empty()) {
		return(0);
	} else {
		tokens_count = tokens->size();
	}

	token = (*tokens)[0];

	if (token == "stop" || token == "quit" ||
		token == "exit" || token == "q" ) {
		if (tokens_count != 1) {
			return(-1);
		} else {
			return(1);
		}
	} else if (token == "help") {
		puts(". , abs add cos div dump dup");
		puts("lda ldb ldc ldd ln log log2");
		puts("mod mul neg new over pi pop");
		puts("pow push sin sqrt sta stb");
		puts("stc std sub sum swap tan top");
	} else if (token == "push") {
		if (tokens_count != 2)
			return(-1);
		long double number = atof((*tokens)[1].c_str());
		STACK.push(number);
	} else if (token == "pi") {
		if (tokens_count != 1)
			return(-1);
		STACK.push(PI);
	} else if (token == "lda") {
		if (tokens_count != 1)
			return(-1);
		STACK.push(reg_a);
	} else if (token == "ldb") {
		if (tokens_count != 1)
			return(-1);
		STACK.push(reg_b);
	} else if (token == "ldc") {
		if (tokens_count != 1)
			return(-1);
		STACK.push(reg_c);
	} else if (token == "ldd") {
		if (tokens_count != 1)
			return(-1);
		STACK.push(reg_d);
	} else if (token == "sta") {
		if (tokens_count != 1)
			return(-1);
		reg_a = STACK.top();
		STACK.pop();
	} else if (token == "stb") {
		if (tokens_count != 1)
			return(-1);
		reg_b = STACK.top();
		STACK.pop();
	} else if (token == "stc") {
		if (tokens_count != 1)
			return(-1);
		reg_c = STACK.top();
		STACK.pop();
	} else if (token == "std") {
		if (tokens_count != 1)
			return(-1);
		reg_d = STACK.top();
		STACK.pop();
	} else if (token == "pop" || token == ",") {
		if (tokens_count != 1)
			return(-1);
		if (STACK.empty()) {
			puts("~");
		} else {
			STACK.pop();
		}
	} else if (token == "swap") {
		if (tokens_count != 1) return(-1);
		if (STACK.size() < 2) {
			puts("!OPS");
		} else {
			long double a = 0;
			long double b = 0;
			a = STACK.top();
			STACK.pop();
			b = STACK.top();
			STACK.pop();
			STACK.push(a);
			STACK.push(b);
		}
	} else if (token == "over") {
		if (tokens_count != 1) return(-1);
		if (STACK.size() < 2) {
			puts("!OPS");
		} else {
			long double a = 0;
			long double b = 0;
			a = STACK.top();
			STACK.pop();
			b = STACK.top();
			STACK.push(a);
			STACK.push(b);
		}
	} else if (token == "dup") {
		if (tokens_count != 1)
			return(-1);
		if (STACK.size() == 0) {
			puts("~");
		} else {
			STACK.push(STACK.top());
		}
	} else if (token == "top" || token == ".") {
		if (tokens_count != 1)
			return(-1);
		if (STACK.size() == 0) {
			puts("~");
		} else {
			printf("%Lf\n", STACK.top());
		}
	} else if (token == "neg") {
		if (tokens_count != 1) return(-1);
		if (STACK.size() < 1) {
			puts("!OPS");
		} else {
			long double a = 0;
			a = STACK.top();
			STACK.pop();
			STACK.push(-a);
		}
	} else if (token == "add") {
		if (tokens_count != 1) return(-1);
		if (STACK.size() < 2) {
			puts("!OPS");
		} else {
			long double a = 0;
			long double b = 0;
			a = STACK.top();
			STACK.pop();
			b = STACK.top();
			STACK.pop();
			a = a + b;
			STACK.push(a);
		}
	} else if (token == "sub") {
		if (tokens_count != 1) return(-1);
		if (STACK.size() < 2) {
			puts("!OPS");
		} else {
			long double a = 0;
			long double b = 0;
			a = STACK.top();
			STACK.pop();
			b = STACK.top();
			STACK.pop();
			a = b - a;
			STACK.push(a);
		}
	} else if (token == "mul") {
		if (tokens_count != 1) return(-1);
		if (STACK.size() < 2) {
			puts("!OPS");
		} else {
			long double a = 0;
			long double b = 0;
			a = STACK.top();
			STACK.pop();
			b = STACK.top();
			STACK.pop();
			a = b * a;
			STACK.push(a);
		}
	} else if (token == "div") {
		if (tokens_count != 1) return(-1);
		if (STACK.size() < 2) {
			puts("!OPS");
		} else {
			long double a = 0;
			long double b = 0;
			a = STACK.top();
			STACK.pop();
			b = STACK.top();
			STACK.pop();
			a = b / a;
			STACK.push(a);
		}
	} else if (token == "mod") {
		if (tokens_count != 1) return(-1);
		if (STACK.size() < 2) {
			puts("!OPS");
		} else {
			long double a = 0;
			long double b = 0;
			a = STACK.top();
			STACK.pop();
			b = STACK.top();
			STACK.pop();
			a = static_cast<double>(
					static_cast<int>(b)
					%
					static_cast<int>(a)
					);
			STACK.push(a);
		}
	} else if (token == "sqrt") {
		if (tokens_count != 1)
			return(-1);
		if (STACK.size() < 1) {
			puts("!OPS");
		} else {
			long double a = 0;
			a = STACK.top();
			STACK.pop();
			a = sqrt(a);
			STACK.push(a);
		}
	} else if (token == "pow") {
		if (tokens_count != 1)
			return(-1);
		if (STACK.size() < 1) {
			puts("!OPS");
		} else {
			long double a = 0;
			long double b = 0;
			a = STACK.top();
			STACK.pop();
			b = STACK.top();
			STACK.pop();
			a = powl(b, a);
			STACK.push(a);
		}
	} else if (token == "ln") {
		if (tokens_count != 1) return(-1);
		if (STACK.size() < 1) {
			puts("!OPS");
		} else {
			long double a = 0;
			a = STACK.top();
			STACK.pop();
			a = log1pl(a);
			STACK.push(a);
		}
	} else if (token == "log") {
		if (tokens_count != 1) return(-1);
		if (STACK.size() < 1) {
			puts("!OPS");
		} else {
			long double a = 0;
			a = STACK.top();
			STACK.pop();
			a = log10l(a);
			STACK.push(a);
		}
	} else if (token == "log2") {
		if (tokens_count != 1) {
			return(-1);
		}
		if (STACK.size() < 1) {
			puts("!OPS");
		} else {
			long double a = 0;
			a = STACK.top();
			STACK.pop();
			a = logl(a)/logl(2);
			STACK.push(a);
		}
	} else if (token == "sin") {
		if (tokens_count != 1) {
			return(-1);
		}
		if (STACK.size() < 1) {
			puts("!OPS");
		} else {
			long double a = 0;
			a = STACK.top();
			STACK.pop();
			a = sinl(a);
			STACK.push(a);
		}
	} else if (token == "cos") {
		if (tokens_count != 1) {
			return(-1);
		}
		if (STACK.size() < 1) {
			puts("!OPS");
		} else {
			long double a = 0;
			a = STACK.top();
			STACK.pop();
			a = cosl(a);
			STACK.push(a);
		}
	} else if (token == "tan") {
		if (tokens_count != 1) {
			return(-1);
		}
		if (STACK.size() < 1) {
			puts("!OPS");
		} else {
			long double a = 0;
			a = STACK.top();
			STACK.pop();
			a = tanl(a);
			STACK.push(a);
		}
	} else if (token == "abs") {
		if (tokens_count != 1) return(-1);
		if (STACK.size() < 1) {
			puts("!OPS");
		} else {
			long double a = 0;
			a = STACK.top();
			STACK.pop();
			a = abs(a);
			STACK.push(a);
		}
	} else if (token == "dump") {
		if (tokens_count != 1) return(-1);
		if (STACK.size() == 0) {
			puts("~");
		} else {
			stack<long double> tmp;
			// dump stack
			while (!STACK.empty()) {
				printf("%Lf\n", STACK.top());
				tmp.push(STACK.top());
				STACK.pop();
			}
			puts("~");
			// recover stack
			while (!tmp.empty()) {
				STACK.push(tmp.top());
				tmp.pop();
			}
		}
	} else if (token == "sum") {
		if (tokens_count != 1) return(-1);
		if (STACK.size() == 0) {
			puts("~");
		} else {
			long double a = 0;
			while (!STACK.empty()) {
				a += STACK.top();
				STACK.pop();
			}
			STACK.push(a);
		}
	} else if (token == "new") {
		if (tokens_count != 1) {
			return(-1);
		}
		while (!STACK.empty()) {
			STACK.pop();
		}
	} else {
		if (tokens_count != 1) {
			return(-1);
		}
		long double a = atof(token.c_str());
		STACK.push(a);
	}
	return(0);
}

int main(void)
{
	vector<string> tokens;
	int retval;
	char cmd[CMDSIZE];

	do {
		fflush(stdout);
		memset(cmd, 0, CMDSIZE);
		fgets(cmd, CMDSIZE, stdin);
		tokens.clear();
		retval = parse(cmd, &tokens);
		if (retval == -1) {
			puts("?");
			continue;
		}
		retval = process(&tokens);
		if (retval == -1) {
			puts("?");
			continue;
		}
	} while (retval != 1);
	return(0);
}
