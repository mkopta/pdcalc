CXX      = g++
CXXFLAGS = -Wall -Wextra -pedantic -ansi
name     = pdcalc
args     =

all: build

build: $(name)

$(name): $(name).cc
	$(CXX) $(CXXFLAGS) -o $(name) $(name).cc

run:
	./$(name) $(args)

install: $(name)
	cp $(name) /usr/local/bin/

uninstall:
	rm -f /usr/local/bin/$(name)

clean:
	rm -f $(name)
